package com.example.hiltexample.di

import com.example.hiltexample.repositories.DataRepositories
import com.example.hiltexample.usecase.UseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent


@InstallIn(ActivityRetainedComponent::class)
@Module
object UseCaseModule {
    @Provides
    fun provideUseCase(dataRepositories: DataRepositories): UseCase {
        return UseCase(dataRepositories)
    }
}