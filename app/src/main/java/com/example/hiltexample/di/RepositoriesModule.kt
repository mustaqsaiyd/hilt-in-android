package com.example.hiltexample.di

import com.example.hiltexample.network.ApiServices
import com.example.hiltexample.repositories.DataRepositories
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@InstallIn(ActivityRetainedComponent::class)
@Module
object RepositoriesModule {

    @Provides
    fun provideDataRepo(apiServices: ApiServices): DataRepositories {
        return DataRepositories(apiServices)
    }

}