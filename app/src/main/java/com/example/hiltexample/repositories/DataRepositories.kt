package com.example.hiltexample.repositories

import com.example.hiltexample.mode.GitRepoModel
import com.example.hiltexample.network.ApiServices
import javax.inject.Inject


class DataRepositories
@Inject constructor(private val apiServices: ApiServices) {
    suspend fun getRepo(): GitRepoModel {
        return apiServices.getData()
    }

}