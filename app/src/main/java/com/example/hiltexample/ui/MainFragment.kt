package com.example.hiltexample.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.hiltexample.R
import com.example.hiltexample.mode.ResultData
import com.example.hiltexample.ui.adapter.AdapterRepoList
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.main_fragment.*

@AndroidEntryPoint
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val mainViewModel by viewModels<MainViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val adapterRepoList=AdapterRepoList()
        rvRepo.adapter=adapterRepoList

        val repoList = mainViewModel.getRepoiMain()

        repoList.observe(viewLifecycleOwner, Observer { responseData ->

            when (responseData) {
                is ResultData.Loading -> {

                }
                is ResultData.Success -> {
                    val githubData=responseData.data
                    adapterRepoList.submitList(githubData)
                }
                is ResultData.Failed -> {

                }
                is ResultData.Exception -> {

                }
            }

        })

    }

}