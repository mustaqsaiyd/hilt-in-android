package com.example.hiltexample.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.hiltexample.R
import com.example.hiltexample.mode.DiffUtilsGitRepoModel
import com.example.hiltexample.mode.GitRepoModel

class AdapterRepoList :
    ListAdapter<GitRepoModel.GitRepoModelItem, RepoViewHolder>(DiffUtilsGitRepoModel()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_repo, parent, false)
        return RepoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        val list=getItem(position)
        holder.bind(list)
    }
}