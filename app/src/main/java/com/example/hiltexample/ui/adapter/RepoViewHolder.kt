package com.example.hiltexample.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.hiltexample.mode.GitRepoModel
import kotlinx.android.synthetic.main.item_repo.view.*

class RepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(list: GitRepoModel.GitRepoModelItem?) {
        itemView.tvId.text = list!!.id.toString()
        itemView.tvName.text = list.name
        itemView.tvFullname.text=list.fullName
        Glide.with(itemView.context)
            .load(list.owner.avatarUrl)
            .circleCrop()
            .into(itemView.imgUser)
    }

}