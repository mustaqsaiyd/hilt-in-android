package com.example.hiltexample.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.hiltexample.mode.GitRepoModel
import com.example.hiltexample.mode.ResultData
import com.example.hiltexample.usecase.UseCase

class MainViewModel @ViewModelInject constructor(private val useCase: UseCase) : ViewModel() {

    fun getRepoiMain(): LiveData<ResultData<GitRepoModel>> {
        return liveData<ResultData<GitRepoModel>> {
            emit(ResultData.Loading())
            emit(useCase.getRepoList())
        }
    }

}