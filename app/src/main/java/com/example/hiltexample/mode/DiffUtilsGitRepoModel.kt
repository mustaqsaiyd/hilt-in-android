package com.example.hiltexample.mode

import androidx.recyclerview.widget.DiffUtil

class DiffUtilsGitRepoModel : DiffUtil.ItemCallback<GitRepoModel.GitRepoModelItem>() {
    override fun areItemsTheSame(
        oldItem: GitRepoModel.GitRepoModelItem,
        newItem: GitRepoModel.GitRepoModelItem
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: GitRepoModel.GitRepoModelItem,
        newItem: GitRepoModel.GitRepoModelItem
    ): Boolean {
        return newItem==oldItem
    }


}