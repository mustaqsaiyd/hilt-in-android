package com.example.hiltexample.usecase

import com.example.hiltexample.mode.GitRepoModel
import com.example.hiltexample.mode.ResultData
import com.example.hiltexample.repositories.DataRepositories
import javax.inject.Inject

class UseCase @Inject constructor(private val dataRepositories: DataRepositories) {
    suspend fun getRepoList(): ResultData<GitRepoModel> {
        val repoResult = dataRepositories.getRepo()
        //val repoCode = dataRepositories.getRepo().hashCode()
        val result = if (repoResult.isNotEmpty()) {
            ResultData.Success(repoResult)
        } else {
            ResultData.Failed("Something Went wrong")
        }
        return result
    }
}