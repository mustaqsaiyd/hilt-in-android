package com.example.hiltexample.network

import com.example.hiltexample.mode.GitRepoModel
import com.example.hiltexample.utility.NetworkConstant.REPOSITORY
import retrofit2.http.GET

interface ApiServices {

    @GET(REPOSITORY)
    suspend  fun getData():GitRepoModel

}